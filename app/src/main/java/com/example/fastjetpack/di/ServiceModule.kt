package com.example.fastjetpack.di

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent

@Module
@InstallIn(ApplicationComponent::class)
abstract class ServiceModule {
//    @ContributesAndroidInjector
//    abstract AuthenticatorService authenticatorServiceInjector();
}