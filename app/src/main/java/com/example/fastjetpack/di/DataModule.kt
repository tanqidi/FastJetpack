package com.example.fastjetpack.di

import com.example.fastjetpack.data.dynamic.remote.RemoteDynamicResource
import com.example.fastjetpack.data.notifications.remote.RemoteNotificationsSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit

/**
 * 资源库依赖
 */
@Module
@InstallIn(ApplicationComponent::class)
object DataModule {

    @Provides
    fun remoteNotificationsSource(retrofit: Retrofit): RemoteNotificationsSource {
        return retrofit.create(RemoteNotificationsSource::class.java)
    }

    @Provides
    fun remoteDynamicResource(retrofit: Retrofit): RemoteDynamicResource {
        return retrofit.create(RemoteDynamicResource::class.java)
    }
}