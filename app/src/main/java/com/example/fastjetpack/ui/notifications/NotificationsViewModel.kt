package com.example.fastjetpack.ui.notifications

import android.app.Application
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.fastjetpack.data.notifications.Notification
import com.example.fastjetpack.data.notifications.NotificationRepository
import com.luck.picture.lib.tools.ToastUtils
import kotlinx.coroutines.launch

class NotificationsViewModel @ViewModelInject constructor(
    private val repository: NotificationRepository,
    application: Application
) : AndroidViewModel(application) {

    private val _text = MutableLiveData<String>().apply {
        value = "This is notifications Fragment"
    }
    val text: LiveData<String> = _text

    val notificationsLiveData = MutableLiveData<List<Notification>>()

    fun listNotificationByKeyword(keyword: String?) {
        viewModelScope.launch {
            kotlin.runCatching {
                val notifications = repository.listNotificationByKeyword(keyword)
                notificationsLiveData.postValue(notifications)
            }.onFailure {
                ToastUtils.s(getApplication(), it.message)
            }
        }
    }
}