package com.example.fastjetpack

object AppConstant {
    const val DATE_FORMAT_STYLE = "yyyy-MM-dd"
    const val DATE_TIME_FORMAT_STYLE = "yyyy-MM-dd HH:mm"
    const val BASIC_URL = "http://192.168.0.186:8080/project/"
}