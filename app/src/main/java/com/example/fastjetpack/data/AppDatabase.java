package com.example.fastjetpack.data;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.fastjetpack.data.dynamic.domain.Dynamic;
import com.example.fastjetpack.data.dynamic.local.DynamicDao;

@Database(entities = {Dynamic.class}, version = 1, exportSchema = false)
public abstract  class AppDatabase extends RoomDatabase {
    public abstract DynamicDao dynamicDao();
}
