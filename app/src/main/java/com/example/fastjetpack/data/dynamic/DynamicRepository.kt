package com.example.fastjetpack.data.dynamic

import com.example.fastjetpack.data.dynamic.domain.Dynamic
import com.example.fastjetpack.data.dynamic.remote.RemoteDynamicResource
import io.reactivex.Observable
import javax.inject.Inject

class DynamicRepository @Inject constructor(private val remoteDynamicResource: RemoteDynamicResource) {
    fun list(keyword: String?, page: Int, size: Int): Observable<List<Dynamic>> {
        return remoteDynamicResource.list(keyword, page, size)
    }
}