package com.example.fastjetpack.data;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.annotation.Nullable;

import retrofit2.CallAdapter;
import retrofit2.Retrofit;

public class ResponseMessageAdapterFactory extends CallAdapter.Factory {

    @Nullable
    @Override
    public CallAdapter<?, ?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        Type observableType = getParameterUpperBound(0, (ParameterizedType) returnType);
        Type responseType = getParameterUpperBound(0, (ParameterizedType) observableType);
        return null;
    }

}
