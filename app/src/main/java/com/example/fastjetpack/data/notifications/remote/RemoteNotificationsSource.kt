package com.example.fastjetpack.data.notifications.remote

import com.example.fastjetpack.data.notifications.Notification
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteNotificationsSource {

    @GET("xxx/xxx/listNotificationByKeyword")
    suspend fun listNotificationByKeyword(@Query("keyword") keyword: String?): List<Notification>

}