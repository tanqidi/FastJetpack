package com.example.fastjetpack.data.notifications

import com.example.fastjetpack.data.notifications.remote.RemoteNotificationsSource
import javax.inject.Inject

class NotificationRepository @Inject constructor(
    private val remoteNotificationsSource: RemoteNotificationsSource
) {

    suspend fun listNotificationByKeyword(keyword: String?): List<Notification> {
        return remoteNotificationsSource.listNotificationByKeyword(keyword)
    }

}