package com.example.fastjetpack.data;

import java.io.Serializable;

public class ErrorMessage implements Serializable {

    /**
     * useless
     */
    private Object header;

    /**
     * 信息
     */
    private String body;

    public Object getHeader() {
        return header;
    }

    public void setHeader(Object header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
